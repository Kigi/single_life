#include "DxLib.h"

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	unsigned int Cr;
	ChangeWindowMode(TRUE);
	if (DxLib_Init() == -1) return -1;

	Cr = GetColor(255, 255, 255);
	DrawString(250, 240 - 32, "Hello C World!", Cr);

	WaitKey();
	DxLib_End(); 

	return 0;
}
