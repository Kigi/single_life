#ifndef K
#define K

/*-----------------------------------------------------
schedule_data構造体　schedt
	・説明
		スケジュールのデータ保存用構造体
-----------------------------------------------------*/
typedef struct K_schedule_data{
	int sche_day;     //スケジュールの日付データ
	int sche_type;    //スケジュールの種類データ
	int sche_hour;    //スケジュールの時間データ
	int sche_min;	 //スケジュールの分データ
	char sche_title[20];  //スケジュールのタイトルデータ
	char sche_cont[200];	 //スケジュールの内容データ
} K_schedt;

#endif

/* 以下関数宣言 */
int K_p_calendar(int year, int mon);
int K_p_schedule(int year, int mon, K_schedt *data);
