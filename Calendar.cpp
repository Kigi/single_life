#include <stdio.h>
#include "K.h"

/*-----------------------------------------------------
int型関数 K_p_calendar
	・説明
		参照したい月のカレンダーを表示する。CUI用

	・返り値
		0：正常終了
		1：データにエラーがある

	・引数
		int year
			参照したいスケジュールの西暦
		int mon
			参照したいスケジュールの月
-----------------------------------------------------*/
int K_p_calendar(int year, int mon)
{
	int j, k, mod, hi[4] = { 28, 29, 30, 31 };

	//ツェラーの公式再現
	if (mon == 1 || mon == 2)
	{
		j = (year - 1) / 100;
		k = (year - 1) % 100;

		mod = 1 + ((mon + 13) * 26 / 10) + k + (k / 4) + (j / 4) + 5 * j;
		mod = mod % 7;
	}
	else
	{
		j = year / 100;
		k = year % 100;

		mod = 1 + ((mon + 1) * 26 / 10) + k + (k / 4) + (j / 4) + 5 * j;
		mod = mod % 7;
	}

	mod--;
	if (mod == -1)
		mod = 6;

	//日にち決定
	if (mon == 1 || mon == 3 || mon == 5 || mon == 7 || mon == 8 || mon == 10 || mon == 12)
		j = 3;
	else if (mon == 4 || mon == 6 || mon == 9 || mon == 11)
		j = 2;
	else if ((year % 4 == 0 && year % 4 != 0) || year % 400 == 0)
		j = 1;
	else
		j = 0;

	//カレンダー作り
	printf("\n\n%d 年 %d 月のカレンダー\n", year, mon);
	printf("----------------------------\n");
	printf(" Sun Mon Tue Wed Thu Fei Sat\n");
	printf("----------------------------\n");

	//カレンダー空白部分
	for (k = 0; k < mod; k++)
	{
		printf("    ");
	}

	//カレンダー日にち部分
	k = 1;
	while (k <= hi[j])
	{
		if (mod < 7)
		{
			printf("%4d", k);
			mod++;
			k++;
		}
		else
		{
			printf("\n");
			mod = 0;
		}
	}

	printf("\n----------------------------\n");

	return 0;
}

/*-----------------------------------------------------
int型関数 K_r_schedule
	・説明
		データからスケジュールを読み込む

	・引数
		int year
			参照したいスケジュールの西暦
		int mon
			参照したいスケジュールの月
		K_schedt *data
			読み込んだデータを保存するK_schedt構造体の先頭アドレス

	・返り値
		0：データなし
		1：データの個数
-----------------------------------------------------*/
int K_r_schedule(int year, int mon, K_schedt *data)
{
	int i, day, type, hour, min,;
	char file_name[12], *r_cp, 
		title[21], cont[201];
	FILE *fp;

	//ファイルネーム作成用
	sprintf(file_name, "%d_%d.csv", year, mon);
	
	//対象の年と月のスケジュールデータがあるかどうか判断
	if ((fp = fopen(file_name, "r")) == NULL) { return 0; }

	while (fgets(r_cp, 230, fp) != NULL) {
		sscanf(r_cp, "%d,%d,%d,%d,%[^,],%s", &day, &type, &hour, &min, title, cont);
		printf("日付：%d\n", day);
		printf("種類：%d\n", type);
		printf("時間： %02d:%02d", hour, min);
		printf("タイトル：%s\n", title);
		printf("内容：%s\n", cont);

		data[i] ->  
		i++;
	}

}